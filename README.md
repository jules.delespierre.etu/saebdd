# Exploitation d'une base de données

## Présentation

Ce travail que j'ai réalisé avec Victor [Dhorme](mailto:victor.dhorme.etu@univ-lille.fr) était une exploitation d'une base de données en CSV, que nous avons du importer dans une base SQL afin de la ventiler et ainsi faire des requêtes sur des critères demandés.